package com.example.deptinfo.barcodebattler;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by hp2015 on 29/10/2017.
 */

public class MonsterAdapter extends BaseAdapter {
    private List<Monster> listm;
    private Context mContext;
    private LayoutInflater mInflater;
    private BdMonster bdMonster;

    public  MonsterAdapter(Context context,List<Monster> listmn){
        mContext=context;
        listm=listmn;
        mInflater=LayoutInflater.from(mContext);
        bdMonster=new BdMonster(mContext);

    }
    @Override
    public int getCount() {
        return listm.size();
    }

    @Override
    public Object getItem(int i) {
        return listm.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup parent) {
        LinearLayout layoutItem;
       //initialisation a partir layout XML monster_itemml
        layoutItem=(LinearLayout) mInflater.inflate(R.layout.monster_item,parent,false);
        //Recupération des textView et Imageview de notre layout
        TextView tv_nom=(TextView) layoutItem.findViewById(R.id.nom);
        TextView tv_point=(TextView) layoutItem.findViewById(R.id.pdv);
        TextView tv_degat=(TextView) layoutItem.findViewById(R.id.degat);
        TextView tv_armur=(TextView) layoutItem.findViewById(R.id.armur);
        ImageView tv_img=(ImageView) layoutItem.findViewById(R.id.img);
        ImageView tv_imgdelete=(ImageView) layoutItem.findViewById(R.id.btdelete);

        //renseignement des valeurs
        tv_nom.setText("Nom : "+listm.get(i).getNom());
        tv_point.setText("VIE : "+listm.get(i).getPointDeVie());
        tv_degat.setText("DEGAT : "+listm.get(i).getDegat());
        tv_armur.setText("ARMUR : "+listm.get(i).getArmur());
        tv_img.setImageResource(listm.get(i).getDrawbaleImg());
        tv_imgdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder adb= new AlertDialog.Builder(view.getContext());
                adb.setTitle("Monster Selectioner");
                        adb.setMessage("Est ce que vous confirmez la supression : "+listm.get(i).getNom());
                adb.setPositiveButton("Supprimer", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        bdMonster.open();
                        bdMonster.removeMonsterWithId(listm.get(i).getId());
                        bdMonster.close();
                        listm.remove(i);
                        notifyDataSetChanged();
                    }
                });
                adb.setNegativeButton("Annuler",null);
                adb.show();
            }
        });


     ;

        return layoutItem;
    }
}
