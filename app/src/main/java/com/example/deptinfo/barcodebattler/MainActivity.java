package com.example.deptinfo.barcodebattler;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.Result;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class MainActivity extends AppCompatActivity {

    ImageView listMonst, localBattel, networkBattel;
    AcceptThread at;
    private static final int REQUEST_ENABLE_BT = 999;
    private ZXingScannerView scannerView;
    Handler handle;
    BdMonster bd;
    MediaPlayer mp;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA,Manifest.permission.BLUETOOTH}, 0);
        handle = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Log.d("LOG", msg.toString());
                Toast.makeText(MainActivity.this,msg.toString(),Toast.LENGTH_LONG).show();
                //  Toast.makeText(MainActivity.this, msg.toString(), Toast.LENGTH_LONG).show();
            }
        };
        mp = MediaPlayer.create(this, R.raw.drag);
        mp.start();
        bd = new BdMonster(this);
        listMonst = (ImageView) findViewById(R.id.img1);
        localBattel = (ImageView) findViewById(R.id.img2);
        networkBattel = (ImageView) findViewById(R.id.img3);
        listMonst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           mp.start();
                Intent listMonst = new Intent(MainActivity.this, AllMonsters.class);
                startActivity(listMonst);
            }
        });
        localBattel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                Intent listMonst = new Intent(MainActivity.this, CombatLocal.class);
                startActivity(listMonst);
            }
        });
        networkBattel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mp.start();
                BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if (mBluetoothAdapter == null) {            //CREER ACCEPT THREAD
                    // Device does not support Bluetooth
                }
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }

                MainActivity.this.at = new AcceptThread(mBluetoothAdapter, handle);
                at.start();
                Intent listMonst = new Intent(MainActivity.this, CombatNetwork.class);
                startActivity(listMonst);

            }
        });
    }

    public void scanCode(View view)

    {
        mp.stop();
        scannerView = new ZXingScannerView(this);
        scannerView.setResultHandler(new ZXingScannerResultHandler());
        setContentView(scannerView);
        scannerView.startCamera();
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onPause() {
        super.onPause();
        // scannerView.stopCamera();
    }


    class ZXingScannerResultHandler implements ZXingScannerView.ResultHandler {
        @Override
        public void handleResult(Result result) {
            String resultCode = result.getText();
            String d = md5(resultCode);
            Random r = new Random(d);
            bd.open();
            bd.addMonster(r.creerCreature());
            System.out.println("ALLOOOO"+ r.creerCreature().getHash()+ "  rgale "+d );
            bd.close();
            MainActivity.this.recreate();
            Toast.makeText(MainActivity.this, "Monster Added to List", Toast.LENGTH_SHORT).show();


        }


    }


}