package com.example.deptinfo.barcodebattler;

/**
 * Created by deptinfo on 27/10/2017.
 */

public class Random {

    String info;
    String nom;
    int pointVie;
    int image ;
    int degat;
    int armur;



    public Random(String info) {
        this.info = info;
    }

    public Monster creerCreature() {
        System.out.println(info);

        String testCreatureNom = info.substring(0,3);
        String testCreaturePointVie = info.substring(3,6);
        String testCreatureImage = info.substring(6,9);
        String testCreatureDegat=info.substring(9,12);
        String testCreaturearmur=info.substring(12,15);


        if(Long.parseLong(testCreatureNom, 16)>3500)
            nom="Pikola";
        else if(Long.parseLong(testCreatureNom, 16)>3000)
            nom="Frezer";
        else if(Long.parseLong(testCreatureNom, 16)>2500)
            nom="Vegeta";
        else if(Long.parseLong(testCreatureNom, 16)>2000)
            nom="Tanouti";
        else if(Long.parseLong(testCreatureNom, 16)>1500)
            nom="Goku";
        else if(Long.parseLong(testCreatureNom, 16)>1000)
            nom="azzaoui";
        else if(Long.parseLong(testCreatureNom, 16)>500)
            nom="Gohane";
        else if(Long.parseLong(testCreatureNom, 16)>0)
            nom="teddy";
        //-------------------------------------------------

        if(Long.parseLong(testCreaturePointVie, 16)>2000)
           pointVie=2000;
        else if(Long.parseLong(testCreaturePointVie, 16)>1000)
            pointVie=1500;
        else if(Long.parseLong(testCreaturePointVie, 16)>500)
            pointVie=950;
        else if(Long.parseLong(testCreaturePointVie, 16)>0)
            pointVie=700;
        //-------------------------------------------------
        if(Long.parseLong(testCreatureImage, 16)>3500)
            image=R.drawable.bulba;
        else if(Long.parseLong(testCreatureImage, 16)>3000)
            image=R.drawable.krilin;
        else if(Long.parseLong(testCreatureImage, 16)>2500)
            image=R.drawable.goku;
        else if(Long.parseLong(testCreatureImage, 16)>2000)
            image=R.drawable.fista;
        else if(Long.parseLong(testCreatureImage, 16)>1500)
            image=R.drawable.gohane;
        else if(Long.parseLong(testCreatureImage, 16)>1000)
            image=R.drawable.pika;
        else if(Long.parseLong(testCreatureImage, 16)>500)
            image=R.drawable.vegeta;
        else if(Long.parseLong(testCreatureImage, 16)>0)
            image=R.drawable.hoba;

        //-------------------------------------------------

        if(Long.parseLong(testCreatureDegat, 16)>2000)
            degat=600;
        else if(Long.parseLong(testCreatureDegat, 16)>1000)
            degat=500;
        else if(Long.parseLong(testCreatureDegat, 16)>500)
            degat=250;
        else if(Long.parseLong(testCreatureDegat, 16)>0)
            degat=150;
        //-------------------------------------------------
        if(Long.parseLong(testCreaturearmur, 16)>2000)
            armur=400;
        else if(Long.parseLong(testCreaturearmur, 16)>1000)
            armur=300;
        else if(Long.parseLong(testCreaturearmur, 16)>500)
            armur=200;
        else if(Long.parseLong(testCreaturearmur, 16)>0)
            armur=100;
        //-------------------------------------------------

      Monster m=new Monster(nom,pointVie,image,degat,armur,info);
        return m;
    }

}
