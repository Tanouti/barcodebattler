package com.example.deptinfo.barcodebattler;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.*;
import java.util.Random;

/**
 * Created by deptinfo on 26/10/2017.
 */

public class Monster implements Parcelable {

    int id;
    String nom;
    int pointDeVie;
    int degat;
    int armur;
    int drawbaleImg;
    String hash;
    public Monster(String nom,int pointDeVie,int drawbaleImg,int degat,int armur,String hash)
    {
    this.nom=nom;
    this.pointDeVie=pointDeVie;
    this.drawbaleImg=drawbaleImg;
    this.degat=degat;
    this.armur=armur;
    this.hash = hash;
    }
    public Monster(){

    }

    protected Monster(Parcel in) {
        id = in.readInt();
        nom = in.readString();
        pointDeVie = in.readInt();
        degat = in.readInt();
        armur = in.readInt();
        drawbaleImg = in.readInt();
    }

    public static final Creator<Monster> CREATOR = new Creator<Monster>() {
        @Override
        public Monster createFromParcel(Parcel in) {
            return new Monster(in);
        }

        @Override
        public Monster[] newArray(int size) {
            return new Monster[size];
        }
    };

    public int getDegat() {return degat;}

    public int getArmur() {return armur;}

    public void setDegat(int degat) { this.degat = degat;}

    public void setArmur(int armur) { this.armur = armur;}

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPointDeVie() {
        return pointDeVie;
    }

    public void setPointDeVie(int pointDeVie) {
        this.pointDeVie = pointDeVie;
    }

    public int getDrawbaleImg() {
        return drawbaleImg;
    }

    public void setDrawbaleImg(int drawbaleImg) {
        this.drawbaleImg = drawbaleImg;
    }

    public String getHash() { return hash;}
    public void setHash(String hash) { this.hash=hash;}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int Attaque(Monster m)
    {
        Random rand=new Random();
        int nbalt=rand.nextInt(this.degat);
        int degat=nbalt-m.getArmur();
        if(degat>0)
        m.setPointDeVie(m.getPointDeVie()-degat);
        return degat;
    }
    public boolean Died(){
        if(pointDeVie>0){
            return false;
        }
        else{
            return true;
        }
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(nom);
        parcel.writeInt(pointDeVie);
        parcel.writeInt(degat);
        parcel.writeInt(armur);
        parcel.writeInt(drawbaleImg);
    }
}
